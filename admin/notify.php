<?php

define('IN_DOUCO', true);

require (dirname(__FILE__) . '/include/init.php');
require (ROOT_PATH.'include/notify/notify.func.php');
// rec操作项的初始化
$rec = $check->is_rec($_REQUEST['rec']) ? $_REQUEST['rec'] : 'default';
// 赋值给模板
$smarty->assign('rec', $rec);
$smarty->assign('cur', 'notify');

/**
 * +----------------------------------------------------------
 * 通知模板类型
 * +----------------------------------------------------------
 */
if ($rec == 'default') {
    $smarty->assign('ur_here', '通知模板');
    $smarty->display('notify.htm');
}

/**
 * 通知模板
 */
if($rec == 'template')
{
    $type = $_REQUEST['type']?$_REQUEST['type']:'email';
    $notify_type = $_REQUEST['notify_type']?$_REQUEST['notify_type']:'user';
    //获取会员通知列表
    $template_list = getNoticeTemplateType($type,$notify_type);
    foreach ($template_list as $key=>$template){
        $template_code = $template['template_code'];
        $template_detail = getNoticeTemplateDetail($template_code,$type);
        $template_list[$key]['is_enable'] = $template_detail['is_enable'];
        $template_list[$key]['template_title'] = $template_detail['template_title'];;
        $template_list[$key]['template_content'] =str_replace(PHP_EOL, '', $template_detail["template_content"]);;
        $template_list[$key]['sign_name']= $template_detail["sign_name"]?$template_detail["sign_name"]:$_CFG['sms_sign'];
        $template_list[$key]['notification_mode']= $template_detail["notification_mode"];
    }
    $template_item = getNoticeTemplateItem($template_list[0]["template_code"]);
    $smarty->assign('template_list',$template_list);
    $smarty->assign('template_list_json',json_encode($template_list));
    //默认选中第一个
    $smarty->assign('template_select',$template_list[0]);
    $smarty->assign('template_item',$template_item);
    $smarty->assign('template_item_json',json_encode($template_item));
    $smarty->assign('type',$type);
    $smarty->assign('notify_type',$notify_type);
    $smarty->display('notify.htm');
}

/**
 * AJAX获取通知项的变量
 */
if($rec == 'get_template_item'){

    $template_code = $_REQUEST['template_code'];
    $template_item = getNoticeTemplateItem($template_code);
    exit(json_encode($template_item));
}
/**
 * 更新通知模板
 */
if($rec == 'update'){
    $type = $_REQUEST['type']?$_REQUEST['type']:'email';
    $template_data = stripslashes($_REQUEST['template_data']);
    $notify_type = $_REQUEST['notify_type']?$_REQUEST['notify_type']:'user';
    $result = updateNoticeTemplate($type,$template_data,$notify_type);
    exit('1');
}
?>