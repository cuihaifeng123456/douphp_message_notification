<?php

<?php
/**
 * douphp二次开发类库
 */


/**
 * 发送短信函数
 * @param $template_code    模板code
 * @param $params           变量参数
 * @param $mobile           接收手机号
 */
function send_sms($template_code,$params,$mobile=''){
    global $_CFG;
    require_once  LOADER_PATH.'notify/notify.func.php';
    $template_obj = getNoticeTemplateDetail($template_code, "sms");

    if(!$mobile && $template_obj['notify_type'] == 'business'){
        $mobile = $template_obj['notification_mode'];
    }
    $sms_result=aliSmsSend($_CFG['sms_app_key'],$_CFG['sms_secret_key'],
        $template_obj["sign_name"], json_encode($params), $mobile, $template_obj["template_title"],  $_CFG['sms_user_type']);
    $sms_result = dealAliSmsResult($sms_result);
    if($sms_result['code']<0){
        return false;
    }
    return true;
}



function send_mail($template_code,$params,$email=''){

    global $_CFG,$dou;
    require_once  LOADER_PATH.'notify/notify.func.php';
    $template_obj = getNoticeTemplateDetail($template_code, "email");
    if(!$email && $template_obj['notify_type'] == 'business'){
        $email = $template_obj['notification_mode'];
    }
    $code_list = getNoticeTemplateItem($template_code);
    $content = $template_obj['template_content'];

    foreach($code_list as $key=>$value){
        $content = str_replace($value['show_name'],$params[$value[replace_name]],$content);
    }
    //调用系统自带的发送邮件
    $dou->send_mail($email,$template_obj['template_title'],$content);

    
}


    //生成验证码
    $vcode = random(6);
    $_SESSION['vcode'] = $vcode;
    $_SESSION['vcode_mobile'] = $tel;

    //发送短信
    $result = send_sms('forgot_password',array('code'=>$vcode),$tel);

    if($result){
        result(1, '短信发送成功!');
    }